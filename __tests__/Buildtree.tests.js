import BuildTree from '../src/BuildTree';

describe('Buildtree', () => {
   it('should correctly build map based on filelist', () => {
      expect(
         BuildTree.prototype.generateDataMap(
            '/home/grannyweatherwax/broom\n/home/rincewind/grimoires/world-creation.spell'
         )
      ).toMatchSnapshot();
   });

   it('should correctly sort by name property', () => {
      const result = [{ name: 'Zeeb' }, { name: 'eve' }, { name: 'Abe' }, { name: 'Eve' }].sort(
         BuildTree.prototype.sortByName
      );

      expect(result).toEqual([{ name: 'Abe' }, { name: 'eve' }, { name: 'Eve' }, { name: 'Zeeb' }]);
   });
});
